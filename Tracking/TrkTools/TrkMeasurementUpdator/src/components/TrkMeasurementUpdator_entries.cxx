#include "TrkMeasurementUpdator/KalmanUpdator.h"
#include "TrkMeasurementUpdator/KalmanUpdatorSMatrix.h"
#include "TrkMeasurementUpdator/KalmanWeightUpdator.h"

DECLARE_COMPONENT( Trk::KalmanUpdator )
DECLARE_COMPONENT( Trk::KalmanUpdatorSMatrix )
DECLARE_COMPONENT( Trk::KalmanWeightUpdator )

